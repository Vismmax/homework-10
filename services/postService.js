const { v4: uuidv4 } = require('uuid');
const { PostRepository } = require('../repositories/postRepository');
const { PostValidationService } = require('./postValidationService');

class PostService {
  search(search) {
    const item = PostRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  getPosts() {
    try {
      let posts = PostRepository.getAll();
      if (!posts) {
        throw Error('Posts not found');
      }
      return posts;
    } catch (error) {
      throw Error('Posts not found');
    }
  }

  getPost(postData) {
    try {
      let post = PostRepository.getOne(postData);
      if (!post) {
        throw Error('Post not found');
      }
      return post;
    } catch (error) {
      throw Error('Post not found');
    }
  }

  create(postData) {
    const newPost = {
      ...postData,
      id: uuidv4(),
      createdAt: new Date().toJSON(),
      editedAt: '',
    };
    const data = PostValidationService.removeExcessFields(newPost);
    try {
      let post = PostRepository.create(data);
      console.log('PostRepository.create', post);
      if (!post) {
        throw Error('Post not saved');
      }
      return post;
    } catch (error) {
      throw Error('Post not saved');
    }
  }

  update(id, postData) {
    const newPost = {
      ...postData,
      editedAt: new Date().toJSON(),
    };
    const data = PostValidationService.removeExcessFields(newPost);
    try {
      let post = PostRepository.update(id, data);
      if (!post) {
        throw Error('Post not updated');
      }
      return post;
    } catch (error) {
      throw Error('Post not updated');
    }
  }

  delete(id) {
    try {
      let post = PostRepository.delete(id);
      if (!post.length) {
        throw Error('Post not deleted');
      }
      return post;
    } catch (error) {
      throw Error('Post not deleted');
    }
  }
}

module.exports = new PostService();
