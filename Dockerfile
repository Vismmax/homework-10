FROM node:12-alpine

COPY client ./client
COPY config ./config
COPY middlewares ./middlewares
COPY models ./models
COPY repositories ./repositories
COPY routes ./routes
COPY services ./services
COPY database.json ./
COPY index.js ./
COPY package*.json ./

RUN npm install

CMD ["node", "index.js"]
EXPOSE $PORT